#Author: Shabana
#User Story: MNA-7 Login Functionality

Feature: Login functionalities of your feature
  I want to use this template for my feature file

  Scenario: Title of your scenario
    Given I am in landing page
    When I click on login Menu
    Then verify I am in Employee login Page
    And I enter login Id
    And I enter password
    And click on login button
    Then I will be in HomePage

 